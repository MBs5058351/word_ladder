import unittest
import final 

class TestFinal(unittest.TestCase):
	def test_find(self):
		fname = input("Enter dictionary name: ")
		file = open(fname)
		lines = file.readlines()
		start = input("Enter start word:")
		words = []
		for line in lines:
			word = line.rstrip()
			if len(word) == len(start):
				words.append(word)
		target = input("Enter target word:")
		count = 0
		path = [start]
		seen = {start : True}
		result=final.find(start, words, seen, target, path)
		self.assertEqual(result,True)
		file.close()
		

if __name__ == '__main__':
    unittest.main()

	